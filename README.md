## Скрипт для отправки алертов

Файлы представлены для демонстрации навыков владения Python.

В репозитории представлены 2 скрипта: 

1. **alerts.py** - с его помощью проводится сравнительная проверка показателей основных метрик за последние 15 минут с такой же пятнадцатимиткой вчера. В случае выявления существенных отличий (отличие больше или меньше на 20%), в Telegram отправляется сообщение и график. 

2. **alerts_day_and_week_ago.py** - похож на первый, но сравнивает 15ти минутки за сегодня, вчера и неделю назад.

3. В папке **alert_example** представлен пример одного из алармов из чата.

Файл .gitlab-ci.yml для настройки автоматического запуска скриптов.

Данные для подключения к базе данных скрыты
