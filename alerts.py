import pandas as pd
import telegram
import pandahouse as ph
from datetime import date, timedelta
import io
from read_db.CH import Getch
import sys
import os
import matplotlib.pyplot as plt
import seaborn as sns

sns.set(rc={'figure.figsize':(16,9)}, 
        style="whitegrid")



def check_anomaly(df, metric, threshold=0.3):
    # функция check_anomaly предлагает алгоритм проверки значения на аномальность посредством
    # сравнения интересующего значения со значением в это же время сутки назад
    # при желании алгоритм внутри этой функции можно изменить
    current_ts = df['ts'].max()  # достаем максимальную 15-минутку из датафрейма - ту, которую будем проверять на аномальность
    day_ago_ts = current_ts - pd.DateOffset(days=1)  # достаем такую же 15-минутку сутки назад

    current_value = df[df['ts'] == current_ts][metric].iloc[0] # достаем из датафрейма значение метрики в максимальную 15-минутку
    day_ago_value = df[df['ts'] == day_ago_ts][metric].iloc[0] # достаем из датафрейма значение метрики в такую же 15-минутку сутки назад

    # вычисляем отклонение
    if current_value <= day_ago_value:
        diff = abs(current_value / day_ago_value - 1)
    else:
        diff = abs(day_ago_value / current_value - 1)

    # проверяем больше ли отклонение метрики заданного порога threshold
    # если отклонение больше, то вернем 1, в противном случае 0
    if diff > threshold:
        is_alert = 1
    else:
        is_alert = 0

    return is_alert, current_value, diff



def run_alerts_feed(chat=None):
    chat_id = chat or -655725590
    bot = telegram.Bot(token=os.environ.get("REPORT_BOT_TOKEN"))

    # для удобства построения графиков в запрос можно добавить колонки date, hm
    data = Getch(
        '''
        SELECT
            toStartOfFifteenMinutes(time) AS ts, 
            toDate(ts) AS date, 
            formatDateTime(ts, '%R') AS hm, 
            uniqExact(user_id) AS users,
            uniqExact(post_id) AS posts,
            countIf(user_id, action='view') AS views,
            countIf(user_id, action='like') AS likes,
            countIf(user_id, action='like') / countIf(user_id, action='view') AS ctr
        FROM 
            simulator_20220120.feed_actions
        WHERE 
            ts >=  today() - 1 
            AND
            ts < toStartOfFifteenMinutes(now())
        GROUP BY 
            ts, 
            date, 
            hm
        ORDER BY 
            ts
        ''').df
    
    # Форматирую значение CTR
    data['ctr'] = data.ctr.apply(lambda x: round((x * 100), 2))
    
    # Создаю список с метриками, которые надо проверить
    metrics = ['users', 'posts', 'views', 'likes', 'ctr']
    
    # Прохожу циклом по каждой мтерике
    for metric in metrics:
        # Проверяем метрику на аномальность алгоритмом, описаным внутри функции check_anomaly()
        is_alert, current_value, diff = check_anomaly(data, metric, threshold=0.2) 
        if is_alert:
            msg = ('Значение метрики {metric} новостной ленты:'.format(metric=metric) + '\n' + '\n'
                   'Текущее значение: {current_value}'.format(current_value=current_value) + '\n'
                   'Отклонение от вчерашнего значения: {diff:.2%}'.format(diff=diff))

            plt.tight_layout()

            ax = sns.lineplot( # строим линейный график
                data=data.sort_values(by=['date', 'hm']), # задаем датафрейм для графика
                x="hm", y=metric, # указываем названия колонок в датафрейме для x и y
                hue="date" # задаем "группировку" на графике (для каждого значения date своя линия)
                )

            # этот цикл нужен чтобы разрядить подписи координат по оси Х,
            for ind, label in enumerate(ax.get_xticklabels()): 
                if ind % 15 == 0:
                    label.set_visible(True)
                else:
                    label.set_visible(False)
            ax.set(xlabel='time') # задаем имя оси Х
            ax.set(ylabel=metric) # задаем имя оси У

            ax.set_title('{}'.format(metric)) # задаем заголовок графика
            ax.set(ylim=(0, None)) # задаем лимит для оси У

            # Формируем файловый объект
            plot_object = io.BytesIO()
            ax.figure.savefig(plot_object)
            plot_object.seek(0)
            plot_object.name = '{0}.png'.format(metric)
            plt.close()

            # Отправляем алерт
            bot.sendMessage(chat_id=chat_id, text=msg)
            bot.sendPhoto(chat_id=chat_id, photo=plot_object)


try:
    run_alerts_feed()
except Exception as e:
    print(e)
    
    
    
def run_alerts_messages(chat=None):
    chat_id = chat or -655725590
    bot = telegram.Bot(token=os.environ.get("REPORT_BOT_TOKEN"))

    # для удобства построения графиков в запрос можно добавить колонки date, hm
    data = Getch(
        '''
        SELECT
            toStartOfFifteenMinutes(time) AS ts, 
            toDate(ts) AS date, 
            formatDateTime(ts, '%R') AS hm, 
            uniqExact(user_id) AS users,
            count(user_id) AS messages
        FROM 
            simulator_20220120.message_actions
        WHERE 
            ts >=  today() - 1 
            AND
            ts < toStartOfFifteenMinutes(now())
        GROUP BY 
            ts, 
            date, 
            hm
        ORDER BY 
            ts
        ''').df

    # Создаю список с метриками, которые надо проверить
    metrics = ['users', 'messages']
    
    # Прохожу циклом по каждой мтерике
    for metric in metrics:
        # Проверяем метрику на аномальность алгоритмом, описаным внутри функции check_anomaly()
        is_alert, current_value, diff = check_anomaly(data, metric, threshold=0.2) 
        if is_alert:
            msg = ('Значение метрики {metric} мессенджера:'.format(metric=metric) + '\n' + '\n'
                   'Текущее значение: {current_value}'.format(current_value=current_value) + '\n'
                   'Отклонение от вчерашнего значения: {diff:.2%}'.format(diff=diff))

            plt.tight_layout()

            ax = sns.lineplot( # строим линейный график
                data=data.sort_values(by=['date', 'hm']), # задаем датафрейм для графика
                x="hm", y=metric, # указываем названия колонок в датафрейме для x и y
                hue="date" # задаем "группировку" на графике (для каждого значения date своя линия)
                )

            # этот цикл нужен чтобы разрядить подписи координат по оси Х,
            for ind, label in enumerate(ax.get_xticklabels()): 
                if ind % 15 == 0:
                    label.set_visible(True)
                else:
                    label.set_visible(False)
            ax.set(xlabel='time') # задаем имя оси Х
            ax.set(ylabel=metric) # задаем имя оси У

            ax.set_title('{}'.format(metric)) # задаем заголовок графика
            ax.set(ylim=(0, None)) # задаем лимит для оси У

            # Формируем файловый объект
            plot_object = io.BytesIO()
            ax.figure.savefig(plot_object)
            plot_object.seek(0)
            plot_object.name = '{0}.png'.format(metric)
            plt.close()

            # Отправляем алерт
            bot.sendMessage(chat_id=chat_id, text=msg)
            bot.sendPhoto(chat_id=chat_id, photo=plot_object)


try:
    run_alerts_messages()
except Exception as e:
    print(e)
