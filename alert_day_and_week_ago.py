import pandas as pd
import telegram
import pandahouse as ph
from datetime import date, timedelta
import io
from read_db.CH import Getch
import sys
import os
import matplotlib.pyplot as plt
import seaborn as sns

sns.set(rc={'figure.figsize':(16,9)}, 
        style="whitegrid")



def check_anomaly(df, metric, threshold=0.3):
    # функция check_anomaly предлагает алгоритм проверки значения на аномальность посредством
    # сравнения интересующего значения со значением в это же время сутки назад и неделю назад
    current_ts = df['ts'].max()  # достаем максимальную 15-минутку из датафрейма - ту, которую будем проверять на аномальность
    day_ago_ts = current_ts - pd.DateOffset(days=1)  # достаем такую же 15-минутку сутки назад
    week_ago_ts = current_ts - pd.DateOffset(days=7)  # достаем такую же 15-минутку неделю назад

    current_value = df[df['ts'] == current_ts][metric].iloc[0] # достаем из датафрейма значение метрики в максимальную 15-минутку
    day_ago_value = df[df['ts'] == day_ago_ts][metric].iloc[0] # достаем из датафрейма значение метрики в такую же 15-минутку сутки назад
    week_ago_value = df[df['ts'] == week_ago_ts][metric].iloc[0] # достаем из датафрейма значение метрики в такую же 15-минутку неделю назад
    
    # вычисляем отклонение
    if current_value <= day_ago_value and current_value <= week_ago_value:
        diff_day = abs(current_value / day_ago_value - 1)
        diff_week = abs(current_value / week_ago_value - 1)
    elif current_value <= day_ago_value and current_value > week_ago_value:
        diff_day = abs(current_value / day_ago_value - 1)
        diff_week = abs(week_ago_value / current_value - 1)
    elif current_value > day_ago_value and current_value <= week_ago_value:
        diff_day = abs(day_ago_value / current_value - 1)
        diff_week = abs(current_value / week_ago_value - 1)      
    else:
        diff_day = abs(day_ago_value / current_value - 1)
        diff_week = abs(week_ago_value / current_value - 1)
        
    # проверяем больше ли отклонение метрики заданного порога threshold
    # если отклонение больше, то вернем 1, в противном случае 0
    if diff_day > threshold or diff_week > threshold:
        is_alert = 1
    else:
        is_alert = 0

    return is_alert, current_value, day_ago_value, week_ago_value, diff_day, diff_week



def run_alerts_feed_all(chat=None):
    chat_id = chat or -655725590
    bot = telegram.Bot(token=os.environ.get("REPORT_BOT_TOKEN"))
    # Вытягиваю метрики новостной ленты за сегодня, вчера и неделю назад с разбивкой по 15ти минуткам
    data = Getch(
        '''
        SELECT
            toStartOfFifteenMinutes(time) AS ts, 
            toDate(ts) AS date, 
            formatDateTime(ts, '%R') AS hm, 
            uniqExact(user_id) AS users,
            uniqExact(post_id) AS posts,
            countIf(user_id, action='view') AS views,
            countIf(user_id, action='like') AS likes,
            countIf(user_id, action='like') / countIf(user_id, action='view') AS ctr
        FROM 
            simulator_20220120.feed_actions
        WHERE 
            (ts >=  today() - 1 
            AND
            ts < toStartOfFifteenMinutes(now()))
            OR
            date = today() - 7
        GROUP BY 
            ts, 
            date, 
            hm
        ORDER BY 
            ts
        ''').df
    
    # Форматирую значение CTR
    data['ctr'] = data.ctr.apply(lambda x: round((x * 100), 2))
    
    # Создаю список с метриками, которые надо проверить
    metrics = ['users', 'posts', 'views', 'likes', 'ctr']
    
    # Прохожу циклом по каждой метрике
    for metric in metrics:
        # Проверяем метрику на аномальность алгоритмом, описаным внутри функции check_anomaly_week()
        (is_alert, current_value, 
         day_ago_value, week_ago_value, 
         diff_day, diff_week) = check_anomaly(data, metric, threshold=0.2) 
        
        if is_alert:
            msg = ('Значение метрики {} новостной ленты:'.format(metric.upper()) + '\n' + '\n'
                   'Текущее значение: {}'.format(current_value) + '\n'
                   'Вчера: {}'.format(day_ago_value) + '\n'
                   'Отклонение: {:.2%}'.format(diff_day) + '\n' + '\n'
                   'Неделю назад: {}'.format(week_ago_value) + '\n'
                   'Отклонение: {:.2%}'.format(diff_week) + '\n' + '\n'
                   '@mybobcat, проверь, пожалуйста. Ссылка на дашборд:' + '\n'
                   'https://superset.lab.karpov.courses/superset/dashboard/332/')

            plt.tight_layout()
            
            ax = sns.lineplot( # строим линейный график
                data=data.sort_values(by=['date', 'hm'], ascending=False), # задаем датафрейм для графика
                x="hm", y=metric, # указываем названия колонок в датафрейме для x и y
                hue="date", # задаем "группировку" на графике (для каждого значения date своя линия)
                style='date') # выделяем линии по-разному

            # этот цикл нужен чтобы разрядить подписи координат по оси Х,
            for ind, label in enumerate(ax.get_xticklabels()): 
                if ind % 15 == 0:
                    label.set_visible(True)
                else:
                    label.set_visible(False)
                    
            ax.set(xlabel='time') # задаем имя оси Х
            ax.set(ylabel=metric) # задаем имя оси У

            ax.set_title('{}'.format(metric.upper()), fontsize = 16) # задаем заголовок графика
            ax.set(ylim=(0, None)) # задаем лимит для оси У

            # Формируем файловый объект
            plot_object = io.BytesIO()
            ax.figure.savefig(plot_object)
            plot_object.seek(0)
            plot_object.name = '{0}.png'.format(metric)
            plt.close()

            # Отправляем алерт
            bot.sendMessage(chat_id=chat_id, text=msg)
            bot.sendPhoto(chat_id=chat_id, photo=plot_object)


try:
    run_alerts_feed_all()
except Exception as e:
    print(e)
    

    
def run_alerts_messages_all(chat=None):
    chat_id = chat or -655725590
    bot = telegram.Bot(token=os.environ.get("REPORT_BOT_TOKEN"))
    # Вытягиваю метрики мессенджера за сегодня, вчера и неделю назад с разбивкой по 15ти минуткам
    data = Getch(
        '''
        SELECT
            toStartOfFifteenMinutes(time) AS ts, 
            toDate(ts) AS date, 
            formatDateTime(ts, '%R') AS hm, 
            uniqExact(user_id) AS users,
            count(user_id) AS messages
        FROM 
            simulator_20220120.message_actions
        WHERE 
            (ts >=  today() - 1 
            AND
            ts < toStartOfFifteenMinutes(now()))
            OR
            date = today() - 7
        GROUP BY 
            ts, 
            date, 
            hm
        ORDER BY 
            ts
        ''').df

    # Создаю список с метриками, которые надо проверить
    metrics = ['users', 'messages']
    
    # Прохожу циклом по каждой мтерике
    for metric in metrics:
        # Проверяем метрику на аномальность алгоритмом, описаным внутри функции check_anomaly()
        (is_alert, current_value, 
         day_ago_value, week_ago_value, 
         diff_day, diff_week) = check_anomaly(data, metric, threshold=0.2) 
        
        if is_alert:
            msg = ('Значение метрики {} мессенджера:'.format(metric.upper()) + '\n' + '\n'
                   'Текущее значение: {}'.format(current_value) + '\n'
                   'Вчера: {}'.format(day_ago_value) + '\n'
                   'Отклонение: {:.2%}'.format(diff_day) + '\n' + '\n'
                   'Неделю назад: {}'.format(week_ago_value) + '\n'
                   'Отклонение: {:.2%}'.format(diff_week) + '\n' + '\n'
                   '@mybobcat, проверь, пожалуйста. Ссылка на дашборд:' + '\n'
                   'https://superset.lab.karpov.courses/superset/dashboard/332/')

            plt.tight_layout()

            ax = sns.lineplot( # строим линейный график
                data=data.sort_values(by=['date', 'hm'], ascending=False), # задаем датафрейм для графика
                x="hm", y=metric, # указываем названия колонок в датафрейме для x и y
                hue="date", # задаем "группировку" на графике (для каждого значения date своя линия)
                style='date') # выделяем линии по-разному

            # этот цикл нужен чтобы разрядить подписи координат по оси Х,
            for ind, label in enumerate(ax.get_xticklabels()): 
                if ind % 15 == 0:
                    label.set_visible(True)
                else:
                    label.set_visible(False)
            ax.set(xlabel='time') # задаем имя оси Х
            ax.set(ylabel=metric) # задаем имя оси У

            ax.set_title('{}'.format(metric.upper()), fontsize = 16) # задаем заголовок графика
            ax.set(ylim=(0, None)) # задаем лимит для оси У

            # Формируем файловый объект
            plot_object = io.BytesIO()
            ax.figure.savefig(plot_object)
            plot_object.seek(0)
            plot_object.name = '{0}.png'.format(metric)
            plt.close()

            # Отправляем алерт
            bot.sendMessage(chat_id=chat_id, text=msg)
            bot.sendPhoto(chat_id=chat_id, photo=plot_object)


try:
    run_alerts_messages_all()
except Exception as e:
    print(e)
